﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameCompare.info.Data.Interfaces;
using GameCompare.info.Models;
using GameCompare.info.Models.APIModels;
using GameCompare.info.SteamApiSupport;
using Microsoft.AspNetCore.Identity;
using Quartz;

namespace GameCompare.info.QuartzJobs
{
    public class BringUserDataCurrentJob : IJob
    {
        private readonly ISteamDataRepository _steamDataRepository;

        public BringUserDataCurrentJob(ISteamDataRepository steamDataRepository)
        {
            _steamDataRepository = steamDataRepository;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                await PerformTask(context);
            }
            catch (Exception ex)
            {
                throw new JobExecutionException(ex)
                {
                    RefireImmediately = false
                };
            }
        }

        private async Task PerformTask(IJobExecutionContext context)
        {
            string userId = context.MergedJobDataMap.GetString("UserId");

            await _steamDataRepository.UpdateUsersFriends(userId);
            await _steamDataRepository.UpdateUsersGames(userId);

            var user = _steamDataRepository.SelectSteamInfoByUserId(userId, true);
            foreach (var friendRelationship in user.Friends)
            {
                
                await _steamDataRepository.UpdateUsersGames(friendRelationship.FriendUserId);
            }

            _steamDataRepository.SetUserDataUpdated(userId);
        }
    }
}
