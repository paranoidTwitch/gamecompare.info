﻿var gamesData;
var gamesOwnerData;
var friendData;
var currentUserData;

$(document).ready(function () {
    var gamesDataSourceJson = $("#gameJsonData").val();
    gamesData = JSON.parse(gamesDataSourceJson);

    var gameOwnerDataSourceJson = $("#gameOwnershipJsonData").val();
    gamesOwnerData = JSON.parse(gameOwnerDataSourceJson);

    var friendDataSourceJson = $("#friendJsonData").val();
    friendData = JSON.parse(friendDataSourceJson);

    var currentUserSourceJson = $("#currentUserJsonData").val();
    currentUserData = JSON.parse(currentUserSourceJson);

    RenderGamesData();
    ProcessUserItemClick('0');
    ProcessFilterChange();
});

function RenderGamesData() {
    var gameTileRow = $("#gamesTileRow");

    for (var i = 0; i < gamesData.length; i++) {

        var tileDiv = CreateGameTile(gamesData[i]);

        gameTileRow.append(tileDiv);
    }
}

function CreateGameTile(gameInfo) {
    var colDiv = $("<div></div>");
    colDiv.attr("id", gameInfo.steam_appid);
    colDiv.addClass("col-lg-4");
    colDiv.addClass("col-md-6");
    colDiv.addClass("col-sm-12");
    colDiv.addClass("thumb");
    colDiv.addClass("game");
    colDiv.attr("title", gameInfo.name);

    for (var i = 0; i < gameInfo.categories.length; i++) {
        colDiv.addClass(
            MakeDescriptionClassNameSafe(gameInfo.categories[i].description)
            );
    }

    for (var j = 0; j < gameInfo.genres.length; j++) {
        colDiv.addClass(
            MakeDescriptionClassNameSafe(gameInfo.genres[j].description)
            );
    }

    var tileDiv = $("<img></img>");
    tileDiv.attr("src", gameInfo.header_image);
    tileDiv.attr("alt", gameInfo.name);
    tileDiv.addClass("img-rounded");
    tileDiv.addClass("img-responsive");
    tileDiv.addClass("margin-all-5");
    

    colDiv.append(tileDiv);

    return colDiv;
}

function ProcessFilterChange() {
    var selectedFilters = [];

    $("#filter-selection input:checked").each(function () {
        selectedFilters.push($(this).attr("id"));
    });

    $(".game").each(function () {
        var gameClasses = $(this).attr("class").split(/\s+/);

        //if the game isn't shared keep hiding it
        if ($.inArray("shared-game", gameClasses) < 0) {
            return;
        }

        //if no filters are selected show everything shared
        if (selectedFilters.length == 0) {
            $(this).show();
            return;
        }

        //if filters are applied only show items with all filter components
        for (var i = 0; i < selectedFilters.length; i++) {
            if ($.inArray(selectedFilters[i], gameClasses) < 0) {
                $(this).hide();
                return;
            }
        }

        $(this).show();
    });
}

function ProcessUserItemClick(friendUserId)
{
    SetUserSelectionDropdown(friendUserId);

    if (friendUserId === '0') {
        $(".game").each(function () {
            $(this).addClass("shared-game");
        })
        $(".game").show();
        return;
    }

    $(".game").hide();
    $(".game").each(function () {
        $(this).removeClass("shared-game");
    });

    var sharedGameIds = gamesOwnerData[friendUserId];
    for (var i = 0; i < sharedGameIds.length; i++) {
        $("#" + sharedGameIds[i]).addClass("shared-game");
        $("#" + sharedGameIds[i]).show();
    }

    ProcessFilterChange();
}

function SetUserSelectionDropdown(friendUserId) {
    if (friendUserId === '0') {
        $("#selectedAvatar").attr("src", currentUserData.AvatarMedium);
        $("#selectedPersonaName").text(currentUserData.PersonaName);
        return;
    }

    var friendUserdata = friendData[friendUserId];
    $("#selectedAvatar").attr("src", friendUserdata.AvatarMedium);
    $("#selectedPersonaName").text(friendUserdata.PersonaName);
}

function MakeDescriptionClassNameSafe(nameToProcess){
    return nameToProcess.replace("(", "")
                        .replace(")", "")
                        .replace(" ", "-")
                        .toLowerCase();
}

function ClearGenres() {
    $("#genre-selection input:checked").each(function () {
        $(this).prop("checked", false);
    });

    ProcessFilterChange();
}

function ClearCategories() {
    $("#category-selection input:checked").each(function () {
        $(this).prop("checked", false);
    });

    ProcessFilterChange();
}