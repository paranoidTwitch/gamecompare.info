﻿var redirectUrl;
var statusAjaxUrl;
var retryCount = 0;

$(document).ready(function () {
    redirectUrl = $("#RedirectUrl").val();
    statusAjaxUrl = $("#StatusAjaxUrl").val();
    showWaitingMessage();
    setTimeout(checkStatus, getNextTimeOutMs());
});

function checkStatus() {
    $.ajax({
        type: "GET",
        url: statusAjaxUrl,
        cache: false,
        success: function(data) {
            var responseObject = JSON.parse(data);
            if (responseObject.IsUserDataReady) {
                showAllDoneMessage();
            } else {
                setTimeout(checkStatus, getNextTimeOutMs());
            }
        },
        error: function () {
            showErrorMessage();
        }
    });
}

function getNextTimeOutMs() {
    retryCount++;
    if (retryCount < 5) {
        return 3000;
    } else if(retryCount < 10) {
        return 5000;
    } else if (retryCount < 15) {
        return 10000;
    } else  if (retryCount < 20) {
        return 15000;
    } else if (retryCount < 30) {
        return 30000;
    } else {
        return 60000;
    }
}

function showWaitingMessage() {
    $("#waitForDataMessage").fadeIn(1000);
}

function showAllDoneMessage() {
    $("#waitForDataMessage").hide();
    $("#allDone").fadeIn(1000);
    setTimeout(function () {
        window.location.href = redirectUrl;
    }, 1800);
}

function showErrorMessage() {
    $("#waitForDataMessage").hide();
    $("#error").fadeIn(1000);
}