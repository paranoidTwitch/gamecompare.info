﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using MySQL.Data.Entity.Extensions;

namespace GameCompare.info.Data
{
    public class ApplicationDbContextFactory : IApplicationDbContextFactory
    {
        private readonly DbContextOptions<ApplicationDbContext> _options;

        public ApplicationDbContextFactory(IConfigurationRoot configuration)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionsBuilder.UseMySQL(configuration.GetConnectionString("MySqlConnectionString"));
            _options = optionsBuilder.Options;
        }

        public ApplicationDbContext GetApplicationDbContextInstance()
        {
            return new ApplicationDbContext(_options);
        }
    }
}
