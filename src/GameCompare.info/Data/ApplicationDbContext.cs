﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameCompare.info.Data.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using GameCompare.info.Models;
using GameCompare.info.Models.APIModels;

namespace GameCompare.info.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        //public DbSet<GameDetail>  GameDetails { get; set; }
        public DbSet<GameType> GameTypes { get; set; } 
        public DbSet<ApplicationFact> ApplicationFacts { get; set; } 
        public DbSet<SteamUserInfo> SteamUserInfos { get; set; }
        public DbSet<GameDetail> GameDetails { get; set; }
        public DbSet<GameCategoryInfo> GameCategories { get; set; } 
        public DbSet<GameGenreInfo> GameGenres { get; set; } 
        public DbSet<SteamUserToFriend> SteamUserToFriends { get; set; }
        public DbSet<SteamUserToGame> SteamUserToGames { get; set; }
        public DbSet<GameToGenre> GameToGenres { get; set; }
        public DbSet<GameToCategory> GameToCategories { get; set; }
        

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<GameType>().HasKey(m => m.GameTypeId);
            builder.Entity<ApplicationFact>().HasKey(m => m.ApplicationFactId);
            builder.Entity<GameCategoryInfo>().HasKey(m => m.Id);
            builder.Entity<GameGenreInfo>().HasKey(m => m.Id);
            builder.Entity<GameDetail>().HasKey(m => m.ApplicationId);
            builder.Entity<SteamUserInfo>().HasKey(m => m.UserId);

            builder.Entity<SteamUserToFriend>().HasKey(m => new {m.SteamUserId, m.FriendUserId});

            builder.Entity<SteamUserToFriend>()
                .HasOne(m => m.SteamUser)
                .WithMany(m => m.Friends)
                .HasForeignKey(m => m.SteamUserId);

            builder.Entity<SteamUserToGame>().HasKey(m => new {m.GameApplicationId, m.SteamUserId});

            builder.Entity<SteamUserToGame>()
                .HasOne(m => m.SteamUser)
                .WithMany(m => m.UsersToGames)
                .HasForeignKey(m => m.SteamUserId);

            builder.Entity<SteamUserToGame>()
                .HasOne(m => m.GameDetail)
                .WithMany(m => m.GamesToUsers)
                .HasForeignKey(m => m.GameApplicationId);

            builder.Entity<GameToCategory>().HasKey(m => new {m.GameApplicaitonId, m.GameCategoryId});

            builder.Entity<GameToCategory>()
                .HasOne(m => m.Game)
                .WithMany(m => m.GameToCategories)
                .HasForeignKey(m => m.GameApplicaitonId);

            builder.Entity<GameToCategory>()
                .HasOne(m => m.CategoryInfo)
                .WithMany(m => m.CategoryToGames)
                .HasForeignKey(m => m.GameCategoryId);

            builder.Entity<GameToGenre>().HasKey(m => new {m.GameApplicationId, m.GenereId});

            builder.Entity<GameToGenre>()
                .HasOne(m => m.Game)
                .WithMany(m => m.GameToGenres)
                .HasForeignKey(m => m.GameApplicationId);

            builder.Entity<GameToGenre>()
                .HasOne(m => m.GenreInfo)
                .WithMany(m => m.GenreToGames)
                .HasForeignKey(m => m.GenereId);

            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
