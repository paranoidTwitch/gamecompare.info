﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using GameCompare.info.Models.APIModels;

namespace GameCompare.info.Data.Models
{
    public class GameToCategory
    {
        [Key]
        public int GameApplicaitonId { get; set; }
        public GameDetail Game { get; set; }

        [Key]
        public int GameCategoryId { get; set; }
        public GameCategoryInfo CategoryInfo { get; set; }
    }
}
