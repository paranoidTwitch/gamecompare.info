﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using GameCompare.info.Models.APIModels;

namespace GameCompare.info.Data.Models
{
    public class GameToGenre
    {
        [Key]
        public int GenereId { get; set; }
        public GameGenreInfo GenreInfo { get; set; }

        [Key]
        public int GameApplicationId { get; set; }
        public GameDetail Game { get; set; }
    }
}
