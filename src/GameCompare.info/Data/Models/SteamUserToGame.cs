﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using GameCompare.info.Models.APIModels;

namespace GameCompare.info.Data.Models
{
    public class SteamUserToGame
    {
        [Key]
        public string SteamUserId { get; set; }
        public SteamUserInfo SteamUser { get; set; }

        [Key]
        public int GameApplicationId { get; set; }
        public GameDetail GameDetail { get; set; }
    }
}
