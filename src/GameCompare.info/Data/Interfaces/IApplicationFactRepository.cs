﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameCompare.info.Data.Repositories;

namespace GameCompare.info.Data.Interfaces
{
    public interface IApplicationFactRepository
    {
        T GetApplicationFact<T>(ApplicationFactType factType);
    }
}
