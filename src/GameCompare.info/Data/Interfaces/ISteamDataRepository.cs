﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GameCompare.info.Models.APIModels;

namespace GameCompare.info.Data.Interfaces
{
    public interface ISteamDataRepository
    {
        ApplicationDbContext GetThreadingContext();
        void InsertSteamUserInfo(SteamUserInfo userInfo);
        void InsertSteamUserInfo(ApplicationDbContext context, SteamUserInfo userInfo);
        SteamUserInfo SelectSteamInfoByUserId(string userId, bool populateRelationships);
        SteamUserInfo SelectSteamInfoByUserId(ApplicationDbContext context, string userId, bool populateRelationships);
        SteamUserInfo SelecSteamUserInfoBySteamId(string steam64, bool populateRelationships);
        SteamUserInfo SelecSteamUserInfoBySteamId(ApplicationDbContext context, string steam64, bool populateRelationships);
        bool IsUserDataCurrent(string userId);
        bool IsUserDataCurrent(ApplicationDbContext context, string userId);
        Task UpdateUsersFriends(string userId);
        Task UpdateUsersFriends(ApplicationDbContext context, string userId);
        Task UpdateUsersGames(string userId);
        Task UpdateUsersGames(ApplicationDbContext context, string userId);
        void SetUserDataUpdated(string userId);
        List<GameCategoryInfo> SelectAllCategories();
        List<GameCategoryInfo> SelectAllCategories(ApplicationDbContext context);
        List<GameGenreInfo> SelectAllGeneres();
        List<GameGenreInfo> SelectAllGeneres(ApplicationDbContext context);
        List<GameDetail> SelectGamesOwnedByUser(string userId, bool populateReferences);
        List<GameDetail> SelectGamesOwnedByUser(ApplicationDbContext context, string userId, bool populateReferences);
        Dictionary<string, SteamUserInfo> SelectUsersFriends(string userId);
        Dictionary<string, SteamUserInfo> SelectUsersFriends(ApplicationDbContext context, string userId);
        Dictionary<string, List<int>> SelectFriendsGamesUnion(string userId, List<GameDetail> games);
        Dictionary<string, List<int>> SelectFriendsGamesUnion(ApplicationDbContext context, string userId, List<GameDetail> games);
    }
}