﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameCompare.info.Data
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext GetApplicationDbContextInstance();
    }
}
