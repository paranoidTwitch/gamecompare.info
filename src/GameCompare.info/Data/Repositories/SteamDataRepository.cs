﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameCompare.info.Data.Interfaces;
using GameCompare.info.Data.Models;
using GameCompare.info.Models;
using GameCompare.info.Models.APIModels;
using GameCompare.info.SteamApiSupport;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;

namespace GameCompare.info.Data.Repositories
{
    public class SteamDataRepository : ISteamDataRepository
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;
        private readonly ISteamApi _steamApi;
        private readonly int _dataExpiryDays;
        private readonly IApplicationDbContextFactory _contextFactory;

        public SteamDataRepository(UserManager<ApplicationUser> userManager, ApplicationDbContext context, ISteamApi steamApi, IConfigurationRoot configuration, IApplicationDbContextFactory contextFactory)
        {
            _userManager = userManager;
            _context = context;
            _steamApi = steamApi;
            _contextFactory = contextFactory;

            _dataExpiryDays = configuration.GetSection("ApplicationSettings").GetValue<int>("UserDataExpireDays");
        }

        public ApplicationDbContext GetThreadingContext()
        {
            return _contextFactory.GetApplicationDbContextInstance();
        }

        public void InsertSteamUserInfo(SteamUserInfo userInfo)
        {
            InsertSteamUserInfo(_context, userInfo);
        }

        public void InsertSteamUserInfo(ApplicationDbContext context, SteamUserInfo userInfo)
        {
            context.SteamUserInfos.Add(userInfo);
            context.SaveChanges();
        }

        public SteamUserInfo SelectSteamInfoByUserId(string userId, bool populateRelationships)
        {
            return SelectSteamInfoByUserId(_context, userId, populateRelationships);
        }

        public SteamUserInfo SelectSteamInfoByUserId(ApplicationDbContext context, string userId, bool populateRelationships)
        {
            SteamUserInfo requestedUserInfo = context.SteamUserInfos.SingleOrDefault(m => m.UserId == userId);
            if (populateRelationships)
            {
                SetRelationshipCollections(context, requestedUserInfo);
            }
            return requestedUserInfo;
        }

        public SteamUserInfo SelecSteamUserInfoBySteamId(string steam64, bool populateRelationships)
        {
            return SelecSteamUserInfoBySteamId(_context, steam64, populateRelationships);
        }

        public SteamUserInfo SelecSteamUserInfoBySteamId(ApplicationDbContext context, string steam64, bool populateRelationships)
        {
            SteamUserInfo requestedUserInfo = context.SteamUserInfos.SingleOrDefault(m => m.SteamId == steam64);
            if (populateRelationships)
            {
                SetRelationshipCollections(context, requestedUserInfo);
            }
            return requestedUserInfo;
        }

        private void SetRelationshipCollections(ApplicationDbContext context, SteamUserInfo requestedUserInfo)
        {
            requestedUserInfo.Friends = context.SteamUserToFriends.Where(sui => sui.SteamUserId == requestedUserInfo.UserId).ToList();
            requestedUserInfo.UsersToGames = context.SteamUserToGames.Where(sutg => sutg.SteamUserId == requestedUserInfo.UserId).ToList();
            foreach (var userToGameRelationship in requestedUserInfo.UsersToGames)
            {
                userToGameRelationship.GameDetail =
                    context.GameDetails.SingleOrDefault(
                        game => game.ApplicationId == userToGameRelationship.GameApplicationId);
            }
        }

        public bool IsUserDataCurrent(string userId)
        {
            return IsUserDataCurrent(_context, userId);
        }

        public bool IsUserDataCurrent(ApplicationDbContext context, string userId)
        {
            SteamUserInfo requestedUser = SelectSteamInfoByUserId(context, userId, false);

            if (requestedUser == null)
            {
                return false;
            }

            TimeSpan timeSinceLastUpdate = DateTime.UtcNow - requestedUser.LastDataSyncTime;
            if (timeSinceLastUpdate.Days <= _dataExpiryDays)
            {
                return true;
            }

            return false;
        }

        public async Task UpdateUsersFriends(string userId)
        {
            await UpdateUsersFriends(_context, userId);
        }

        public async Task UpdateUsersFriends(ApplicationDbContext context, string userId)
        {
            SteamUserInfo requestedUser = SelectSteamInfoByUserId(context, userId, true);

            List<SteamFriend> friends = await _steamApi.GetSteamFriendList(requestedUser.SteamId);
            List<string> friendsToQuery = new List<string>();
            List<SteamUserInfo> friendSteamUserInfos = new List<SteamUserInfo>();
            foreach (var friend in friends)
            {
                SteamUserInfo friendUserInfo = SelecSteamUserInfoBySteamId(context, friend.SteamId, false);
                if (friendUserInfo == default(SteamUserInfo))
                {
                    friendsToQuery.Add(friend.SteamId);
                }
                else
                {
                    friendSteamUserInfos.Add(friendUserInfo);
                }
            }

            if (friendsToQuery.Count > 0)
            {
                List<SteamUserInfo> newFriendInfos = await _steamApi.GetSteamUserInfos(friendsToQuery);

                foreach (var newFriend in newFriendInfos)
                {
                    var newUser = new ApplicationUser { UserName = newFriend.PersonaName };
                    var createUserResult = await _userManager.CreateAsync(newUser);

                    if (createUserResult.Succeeded)
                    {
                        newFriend.UserId = newUser.Id;
                        InsertSteamUserInfo(context, newFriend);
                        friendSteamUserInfos.Add(SelectSteamInfoByUserId(context, newUser.Id, false));
                    }
                }
            }

            List<string> usersFriendUserIdList = friendSteamUserInfos.Select(fsui => fsui.UserId).ToList();


            if (requestedUser.Friends != null)
            {
                List<string> friendIdsToRemove = (from friendRelationship in requestedUser.Friends
                                                  where !usersFriendUserIdList.Contains(friendRelationship.FriendUserId)
                                                  select friendRelationship.FriendUserId).ToList();

                List<string> friendIdsToAdd = usersFriendUserIdList.Where(friendUserId => requestedUser.Friends.All(friendRelationship => friendRelationship.FriendUserId != friendUserId)).ToList();

                foreach (var id in friendIdsToAdd)
                {
                    requestedUser.Friends.Add(new SteamUserToFriend()
                    {
                        SteamUserId = userId,
                        FriendUserId = id
                    });
                }

                foreach (var id in friendIdsToRemove)
                {
                    requestedUser.Friends.Remove(requestedUser.Friends.Single(friendRelationship => friendRelationship.FriendUserId == id));
                }

                context.SaveChanges();
            }
            else
            {
                requestedUser.Friends = new List<SteamUserToFriend>();
                requestedUser.Friends.AddRange(usersFriendUserIdList.Select(id => new SteamUserToFriend()
                {
                    SteamUserId = userId,
                    FriendUserId = id
                }));
                context.SaveChanges();
            }
        }

        public async Task UpdateUsersGames(string userId)
        {
            await UpdateUsersGames(_context, userId);
        }

        public async Task UpdateUsersGames(ApplicationDbContext context, string userId)
        {
            SteamUserInfo requestedUserInfo = SelectSteamInfoByUserId(context, userId, true);
            SteamUserGameOwnership gameOwnershipGameDetails = await _steamApi.GetSteamGameOwnership(requestedUserInfo.SteamId);

            if (gameOwnershipGameDetails.Games == null)
            {
                return;
            }

            List<int> currentGameIdentifiers = context.GameDetails.Select(gd => gd.ApplicationId).ToList();
            foreach (SteamOwnershipGameDetail game in gameOwnershipGameDetails.Games)
            {
                
                if (!currentGameIdentifiers.Contains(game.ApplicationId))
                {
                    GameDetail selectedGameDetail = await _steamApi.GetGameDetail(game.ApplicationId.ToString());
                    //check for null steam api does not return some values
                    //TODO: Figure out why some ids fail ex. 6100
                    if (selectedGameDetail == null)
                    {
                        continue;
                    }
                    //recheck sometimes the steam api returns data for a different game than requested, thats fun!
                    //TODO: Figure out why steam return data not consistent with the app id passed in
                    if (!currentGameIdentifiers.Any(id => selectedGameDetail.ApplicationId == id))
                    {
                        try
                        {
                            UpdateGameRecordAndAssociatedRecords(context, selectedGameDetail);
                            currentGameIdentifiers.Add(selectedGameDetail.ApplicationId);
                        }
                        catch
                        {
                            //swallow this error another thread got it already
                        }
                        
                    }
                }

                if (requestedUserInfo.UsersToGames.All(utg => utg.GameApplicationId != game.ApplicationId) &&
                    currentGameIdentifiers.Contains(game.ApplicationId))
                {
                    requestedUserInfo.UsersToGames.Add(new SteamUserToGame()
                    {
                        GameApplicationId = game.ApplicationId,
                        SteamUserId = requestedUserInfo.UserId
                    });
                }
            }

            context.SaveChanges();
        }

        private void UpdateGameRecordAndAssociatedRecords(GameDetail gameToInsert)
        {
            UpdateGameRecordAndAssociatedRecords(_context, gameToInsert);
        }

        private void UpdateGameRecordAndAssociatedRecords(ApplicationDbContext context, GameDetail gameToInsert)
        {
            if (gameToInsert.Categories != null)
            {
                foreach (var categoryInfo in gameToInsert.Categories)
                {
                    if (!context.GameCategories.Any(gc => gc.Id == categoryInfo.Id))
                    {
                        context.GameCategories.Add(categoryInfo);
                    }

                    context.GameToCategories.Add(new GameToCategory()
                    {
                        GameApplicaitonId = gameToInsert.ApplicationId,
                        GameCategoryId = categoryInfo.Id
                    });
                }
            }

            if (gameToInsert.Genres != null)
            {
                foreach (var genre in gameToInsert.Genres)
                {
                    if (!context.GameGenres.Any(gg => gg.Id == genre.Id))
                    {
                        context.GameGenres.Add(genre);
                    }

                    context.GameToGenres.Add(new GameToGenre()
                    {
                        GameApplicationId = gameToInsert.ApplicationId,
                        GenereId = genre.Id
                    });
                }
            }

            context.GameDetails.Add(gameToInsert);
            context.SaveChanges();
        }

        public void SetUserDataUpdated(string userId)
        {
            SteamUserInfo requestedUserInfo = SelectSteamInfoByUserId(userId, false);
            requestedUserInfo.LastDataSyncTime = DateTime.UtcNow;
            _context.SaveChanges();
        }

        public List<GameCategoryInfo> SelectAllCategories()
        {
            return SelectAllCategories(_context);
        }

        public List<GameCategoryInfo> SelectAllCategories(ApplicationDbContext context)
        {
            return context.GameCategories.ToList();
        }

        public List<GameGenreInfo> SelectAllGeneres()
        {
            return SelectAllGeneres(_context);
        }

        public List<GameGenreInfo> SelectAllGeneres(ApplicationDbContext context)
        {
            return context.GameGenres.ToList();
        }

        public List<GameDetail> SelectGamesOwnedByUser(string userId, bool populateReferences)
        {
            return SelectGamesOwnedByUser(_context, userId, populateReferences);
        }

        public List<GameDetail> SelectGamesOwnedByUser(ApplicationDbContext context, string userId, bool populateReferences)
        {
            var ownershipDetails = context.SteamUserToGames.Where(sutg => sutg.SteamUserId == userId);
            List<GameDetail> returnValue =
                ownershipDetails.Select(od => context.GameDetails.Single(gd => gd.ApplicationId == od.GameApplicationId))
                    .ToList();

            if (populateReferences)
            {
                foreach (var game in returnValue)
                {
                    game.GameToCategories = context.GameToCategories.Where(gtc => gtc.GameApplicaitonId == game.ApplicationId).ToList();
                    game.Categories = new List<GameCategoryInfo>();
                    foreach (var relationship in game.GameToCategories)
                    {
                        game.Categories.Add(context.GameCategories.Single(gc => gc.Id == relationship.GameCategoryId));
                    }

                    game.GameToGenres = context.GameToGenres.Where(gtg => gtg.GameApplicationId == game.ApplicationId).ToList();
                    game.Genres = new List<GameGenreInfo>();
                    foreach (var relationship in game.GameToGenres)
                    {
                        game.Genres.Add(context.GameGenres.Single(gg => gg.Id == relationship.GenereId));
                    }
                }
            }

            return returnValue;
        }

        public Dictionary<string, SteamUserInfo> SelectUsersFriends(string userId)
        {
            return SelectUsersFriends(_context, userId);
        }

        public Dictionary<string, SteamUserInfo> SelectUsersFriends(ApplicationDbContext context, string userId)
        {
            var friendRelationships = context.SteamUserToFriends.Where(sutf => sutf.SteamUserId == userId).ToList();

            Dictionary<string, SteamUserInfo> friendSteamUserInfos = new Dictionary<string, SteamUserInfo>();
            foreach (var friendRelationship in friendRelationships)
            {
                var friendUserInfo = SelectSteamInfoByUserId(context, friendRelationship.FriendUserId, false);
                friendSteamUserInfos.Add(friendUserInfo.UserId, friendUserInfo);
            }

            return friendSteamUserInfos;
        }

        public Dictionary<string, List<int>> SelectFriendsGamesUnion(string userId, List<GameDetail> games)
        {
            return SelectFriendsGamesUnion(_context, userId, games);
        }

        public Dictionary<string, List<int>> SelectFriendsGamesUnion(ApplicationDbContext context, string userId, List<GameDetail> games)
        {
            var friendRelationships = context.SteamUserToFriends.Where(sutf => sutf.SteamUserId == userId).ToList();
            
            var ownedGamesApplicationIds = games.Select(g => g.ApplicationId).ToList();

            Dictionary<string,List<int>> returnValue = new Dictionary<string, List<int>>();
            foreach (var friendRelationship in friendRelationships)
            {
                var friendSteamUser = SelectSteamInfoByUserId(context, friendRelationship.FriendUserId, true);
                List<int> sharedGameIds = new List<int>();

                foreach (var game in friendSteamUser.UsersToGames)
                {
                    if (ownedGamesApplicationIds.Contains(game.GameApplicationId))
                    {
                        sharedGameIds.Add(game.GameApplicationId);
                    }
                }

                returnValue.Add(friendSteamUser.UserId, sharedGameIds);
            }

            return returnValue;
        }
    }
}
