﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameCompare.info.Data.Interfaces;
using GameCompare.info.Models;
using Microsoft.ApplicationInsights.AspNetCore.TelemetryInitializers;

namespace GameCompare.info.Data.Repositories
{
    public class ApplicationFactRepository : IApplicationFactRepository
    {
        private readonly ApplicationDbContext _context;

        public ApplicationFactRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public T GetApplicationFact<T>(ApplicationFactType factType)
        {
            switch (factType)
            {
                case ApplicationFactType.RequestCount:
                    ApplicationFact fact = _context.ApplicationFacts.SingleOrDefault(m => m.Key == "RequestCount");
                    return (T)Convert.ChangeType(fact.Value, typeof(T));
                default:
                    throw new NotImplementedException($"Lookup not defined for {factType}.");
            }
            
        }

        public void SetApplicationFact(ApplicationFactType factType, string value)
        {
            switch (factType)
            {
                case ApplicationFactType.RequestCount:
                    ApplicationFact fact = _context.ApplicationFacts.SingleOrDefault(m => m.Key == "RequestCount");
                    fact.Value = value;
                    _context.SaveChanges();
                    break;
                default:
                    throw new NotImplementedException($"Set not defined for {factType}.");
            }
        }
    }

    public enum ApplicationFactType
    {
        RequestCount
    }
}
