﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using GameCompare.info.Data;
using GameCompare.info.Data.Interfaces;
using GameCompare.info.Data.Repositories;
using GameCompare.info.Models;
using MySQL.Data.Entity.Extensions;
using GameCompare.info.DataServices;
using GameCompare.info.DataServices.Interfaces;
using GameCompare.info.QuartzJobs;
using GameCompare.info.QuartzSupport;
using GameCompare.info.SteamApiSupport;
using GameCompare.info.SteamApiSupport.HttpClient;
using Microsoft.AspNetCore.Identity;
using Quartz;
using Quartz.Impl;

namespace GameCompare.info
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)

                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see http://go.microsoft.com/fwlink/?LinkID=532709
                //builder.AddUserSecrets<Startup>();

                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseMySQL(Configuration.GetConnectionString("MySqlConnectionString")));

            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
            {
                options.User.AllowedUserNameCharacters = SystemConstants.AllowedCharacters;
            })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddMvc();

            //configuration
            services.AddSingleton(provider => Configuration);

            //Data interfaces
            services.AddScoped<ISteamDataRepository, SteamDataRepository>();
            services.AddScoped<IApplicationFactRepository, ApplicationFactRepository>();
            

            services.AddScoped<ISteamApi, SteamApi>();
            services.AddScoped<SteamApiHttpClient, SteamApiHttpClient>();

            services.AddScoped<IHomeDataService, HomeDataService>();
            services.AddScoped<IApplicationDbContextFactory, ApplicationDbContextFactory>();

            //Jobs
            services.AddScoped<BringUserDataCurrentJob, BringUserDataCurrentJob>();


            NameValueCollection quartzProperties = new NameValueCollection()
            {
                { "quartz.serializer.type", "json" }
            };

            StdSchedulerFactory factory = new StdSchedulerFactory(quartzProperties);
            var schedulerTask = factory.GetScheduler();
            schedulerTask.Wait();
            var scheduler = schedulerTask.Result;
            scheduler.JobFactory = new QuartzJobFactory(services.BuildServiceProvider());
            scheduler.Start();
            services.AddSingleton(scheduler);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseApplicationInsightsRequestTelemetry();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseForwardedHeaders(new ForwardedHeadersOptions
                {
                    ForwardedHeaders = Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedFor | Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedProto
                });
            }

            app.UseApplicationInsightsExceptionTelemetry();

            app.UseStaticFiles();

            app.UseIdentity();
            app.UseSteamAuthentication();

            // Add external authentication middleware below. To configure them please see http://go.microsoft.com/fwlink/?LinkID=532715

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            //seed data
            //var context = app.ApplicationServices.GetService<ApplicationDbContext>();
            //if (context.ApplicationFacts.SingleOrDefault(m => m.Key == "RequestCount") == null)
            //{
            //    context.ApplicationFacts.Add(new ApplicationFact() {Key = "RequestCount", Value = "0"});
            //    context.SaveChanges();
            //}
            
        }
    }
}
