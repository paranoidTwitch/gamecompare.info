﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GameCompare.info.Models.APIModels;

namespace GameCompare.info.SteamApiSupport
{
    public interface ISteamApi
    {
        Task<List<SteamUserInfo>> GetSteamUserInfos(List<string> steamId64s);
        Task<List<SteamFriend>> GetSteamFriendList(string steamId64);
        Task<SteamUserGameOwnership> GetSteamGameOwnership(string steamId64);
        Task<GameDetail> GetGameDetail(string applicationId);
    }
}