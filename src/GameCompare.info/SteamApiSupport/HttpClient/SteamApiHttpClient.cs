﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using GameCompare.info.Data.Interfaces;
using GameCompare.info.Data.Repositories;
using GameCompare.info.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace GameCompare.info.SteamApiSupport.HttpClient
{
    public class SteamApiHttpClient : System.Net.Http.HttpClient
    {
        private static readonly SemaphoreSlim Semaphore = new SemaphoreSlim(1, 1);
        private readonly int _requestCountMax;
        private readonly int _requestWindowsSeconds;
        private static DateTime _throttleTimeStart;
        private static int _currentRequestCount;

        private readonly IConfigurationRoot _configuration;
        private readonly IApplicationFactRepository _repository;

        public SteamApiHttpClient(IApplicationFactRepository repository, IConfigurationRoot configuration)
        {
            _repository = repository;
            _configuration = configuration;

            base.DefaultRequestHeaders.UserAgent.ParseAdd($"GameCompareInfo/{Microsoft.Extensions.PlatformAbstractions.PlatformServices.Default.Application.ApplicationVersion}");
            base.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            _throttleTimeStart = DateTime.UtcNow;

            _requestCountMax = configuration.GetSection("ApplicationSettings").GetValue<int>("ThrottlingRequestCountMaxPerPeroid");
            _requestWindowsSeconds = configuration.GetSection("ApplicationSettings").GetValue<int>("ThrottlingPeroidLengthSeconds");
        }  

        public async Task<T> GetAsync<T>(string url)
        {
            await Semaphore.WaitAsync();
            try
            {
                if (_throttleTimeStart.AddSeconds(_requestWindowsSeconds) <= DateTime.UtcNow)
                {
                    _throttleTimeStart = DateTime.UtcNow;
                    _currentRequestCount = 0;
                }

                if (_currentRequestCount >= _requestCountMax)
                {
                    DateTime nextWindowStart = _throttleTimeStart.AddSeconds(10);
                    TimeSpan timeToWait = nextWindowStart - DateTime.UtcNow;
                    if (timeToWait.Milliseconds > 0)
                    {
                        await Task.Delay(timeToWait);
                    }
                    _throttleTimeStart = DateTime.UtcNow;
                    _currentRequestCount = 0;
                }

                _currentRequestCount++;
                
                HttpResponseMessage response = await base.GetAsync(url);

                if (!response.IsSuccessStatusCode)
                {
                    await Task.Delay(_requestWindowsSeconds * 1000);
                    response = await base.GetAsync(url);
                    if (!response.IsSuccessStatusCode)
                    {
                        throw new SteamApiException("There was a problem calling the Steam API.");
                    }
                }

                string jsonContent = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<T>(jsonContent);
            }
            finally
            {
                Semaphore.Release();
            }
        }
    }
}
