﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GameCompare.info.Models.APIModels;
using GameCompare.info.SteamApiSupport.HttpClient;
using Microsoft.CodeAnalysis.CSharp;

namespace GameCompare.info.SteamApiSupport
{
    public class SteamApi : ISteamApi
    {
        private readonly SteamApiHttpClient _steamPoweredClient;

        private string SteamApiKey
        {
            get
            {
                return SecretsManager.Secrets["SteamApiKey"];
            }
        }
        
        public SteamApi(SteamApiHttpClient steamPoweredClient)
        {
            _steamPoweredClient = steamPoweredClient;
        }
        
        public async Task<List<SteamUserInfo>> GetSteamUserInfos(List<string> steamId64s)
        {
            string joinedRequestIds = String.Join(",", steamId64s);

            string requestUri = $"http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key={SteamApiKey}&steamids={joinedRequestIds}";

            StandardResponseBase<PlayerInfoResponse> response = await _steamPoweredClient.GetAsync<StandardResponseBase<PlayerInfoResponse>>(requestUri);

            return response.Response.Players;
        }

        public async Task<List<SteamFriend>> GetSteamFriendList(string steamId64)
        {
            string requestUri = $"http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key={SteamApiKey}&steamid={steamId64}&relationship=friend";

            FriendInfoResponse response = await _steamPoweredClient.GetAsync<FriendInfoResponse>(requestUri);

            return response.Collection.FriendsList;
        }

        public async Task<SteamUserGameOwnership> GetSteamGameOwnership(string steamId64)
        {
            string requestUri = $"http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key={SteamApiKey}&steamid={steamId64}";

            StandardResponseBase<SteamUserGameOwnership> response = await _steamPoweredClient.GetAsync<StandardResponseBase<SteamUserGameOwnership>>(requestUri);

            return response.Response;
        }

        public async Task<GameDetail> GetGameDetail(string applicationId)
        {
            string requestUri = $"http://store.steampowered.com/api/appdetails?appids={applicationId}";
            
            Dictionary<string, GameDetailResponse> response = await _steamPoweredClient.GetAsync<Dictionary<string, GameDetailResponse>>(requestUri);

            return response.First().Value.Detail;
        }
    }
}
