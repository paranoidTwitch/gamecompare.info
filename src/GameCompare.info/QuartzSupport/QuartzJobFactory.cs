﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Quartz;
using Quartz.Simpl;
using Quartz.Spi;

namespace GameCompare.info.QuartzSupport
{
    public class QuartzJobFactory : SimpleJobFactory
    {
        protected readonly IServiceProvider Container;

        public QuartzJobFactory(IServiceProvider container)
        {   
            Container = container;
        }

        public override IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            try
            {
                var job = Container.GetService(bundle.JobDetail.JobType) as IJob;
                return job;
            }
            catch (Exception ex)
            {
                throw new SchedulerException(string.Format("Problem while instantiating job '{0}' from the QuartzJobFactory.", bundle.JobDetail.Key), ex);
            }
            
        }
    }
}
