﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameCompare.info.Models.AjaxModels
{
    public class IsUserDataReadyViewModel
    {
        public bool IsUserDataReady { get; set; }
    }
}
