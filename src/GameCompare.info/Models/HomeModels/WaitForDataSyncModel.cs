﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameCompare.info.Models.HomeModels
{
    public class WaitForDataSyncModel
    {
        public string RedirectUrl { get; set; }
        public string StatusAjaxUrl { get; set; }
    }
}
