﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameCompare.info.Models.APIModels;

namespace GameCompare.info.Models.HomeModels
{
    public class HomeIndexViewModel
    {
        public SteamUserInfo CurrentUserInfo { get; set; }
        public Dictionary<string, SteamUserInfo> FriendUserInfos { get; set; } 
        public List<GameCategoryInfo> Categories { get; set; }
        public List<GameGenreInfo> Genres { get; set; }
        public Dictionary<string, List<int>> FriendGameOwnership { get; set; }
        public List<GameDetail> Games { get; set; }
    }
}
