﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace GameCompare.info.Models.APIModels
{
    public class GameDemoInfo
    {
        [Key]
        [JsonProperty("appid")]
        public int DemoApplicationId { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
