﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;

namespace GameCompare.info.Models.APIModels
{
    public class SteamUserGameOwnership
    {
        [JsonProperty("game_count")]
        public int GameCount { get; set; }
        
        [JsonProperty("games")]
        public List<SteamOwnershipGameDetail> Games { get; set; }
    }
}
