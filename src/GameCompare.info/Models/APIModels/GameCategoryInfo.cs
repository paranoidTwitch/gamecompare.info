﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using GameCompare.info.Data.Models;
using Newtonsoft.Json;

namespace GameCompare.info.Models.APIModels
{
    public class GameCategoryInfo
    {
        [Key]
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonIgnore]
        public List<GameToCategory> CategoryToGames { get; set; }
    }
}
