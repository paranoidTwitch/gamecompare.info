﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using GameCompare.info.Data.Models;

namespace GameCompare.info.Models.APIModels
{
    public class GameDetail
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [Key]
        [JsonProperty("steam_appid")]
        public int ApplicationId { get; set; }

        [JsonProperty("is_free")]
        public bool IsFree { get; set; }

        [JsonProperty("header_image")]
        public string HeaderImage { get; set; }

        [JsonProperty("categories")]
        [NotMapped]
        public List<GameCategoryInfo> Categories { get; set; } 

        [JsonProperty("genres")]
        [NotMapped]
        public List<GameGenreInfo> Genres { get; set; }

        [JsonIgnore]
        public List<GameToCategory> GameToCategories { get; set; }

        [JsonIgnore]
        public List<GameToGenre> GameToGenres { get; set; }

        [JsonIgnore]
        public List<SteamUserToGame> GamesToUsers { get; set; } 
    }
}
