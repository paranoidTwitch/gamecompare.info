﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;

namespace GameCompare.info.Models.APIModels
{
    public class SteamOwnershipGameDetail
    {
        [Key]
        [JsonProperty("appid")]
        public int ApplicationId { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("playtime_forever")]
        public int PlaytimeForeverMinutes { get; set; }
        [JsonProperty("playtime_2weeks")]
        public int PlaytimeTwoWeeksMinutes { get; set; }
        [JsonProperty("img_icon_url")]
        public string ImgIconUrl { get; set; }
        [JsonProperty("img_logo_url")]
        public string ImgLogoUrl { get; set; }
        [JsonProperty("has_community_visible_stats")]
        public bool HasCommunityVisibleStats { get; set; }
    }
}
