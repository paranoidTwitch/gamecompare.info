﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameCompare.info.Models.APIModels
{
    public class GameType
    {
        [Key]
        public int GameTypeId { get; set; }
        public string GameTypeName { get; set; }
    }
}
