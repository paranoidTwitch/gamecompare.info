﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GameCompare.info.Data.Models;
using Newtonsoft.Json;

namespace GameCompare.info.Models.APIModels
{
    public class SteamUserInfo
    {
        [Key]
        [JsonIgnore]
        public string UserId { get; set; }
        [JsonIgnore]
        public List<SteamUserToGame> UsersToGames { get; set; }
        [JsonIgnore]
        public List<SteamUserToFriend> Friends { get; set; } 

        public string SteamId { get; set; }
        public int CommunityVisibilityState { get; set; }
        public int ProfileState { get; set; }
        public string PersonaName { get; set; }
        public string LastLogoff { get; set; }
        public string ProfileUrl { get; set; }
        public string Avatar { get; set; }
        public string AvatarMedium { get; set; }
        public string AvatarFull { get; set; }
        public int PersonaState { get; set; }
        public string RealName { get; set; }
        public string PrimaryClanId { get; set; }
        public string TimeCreated { get; set; }
        public int PersonaStateFlags { get; set; }
        public DateTime LastDataSyncTime { get; set; }
    }
}
