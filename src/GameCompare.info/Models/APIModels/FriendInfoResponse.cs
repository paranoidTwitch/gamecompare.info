﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;

namespace GameCompare.info.Models.APIModels
{
    public class FriendInfoResponse
    {
        [JsonProperty("friendslist")]
        public FriendCollection Collection { get; set; }
    }
}
