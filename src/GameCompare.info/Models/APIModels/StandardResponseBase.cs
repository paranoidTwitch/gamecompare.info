﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace GameCompare.info.Models.APIModels
{
    public class StandardResponseBase<T>
    {
        [JsonProperty("response")]
        public T Response { get; set; }
    }
}
