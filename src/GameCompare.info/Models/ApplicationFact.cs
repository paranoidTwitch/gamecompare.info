﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameCompare.info.Models
{
    public class ApplicationFact
    {
        public int ApplicationFactId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
