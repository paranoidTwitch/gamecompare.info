﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace GameCompare.info
{
    public static class SecretsManager
    {
        public static Dictionary<string, string> Secrets { get; private set; } 

        static SecretsManager()
        {
            string secretsJson = File.ReadAllText("application.secrets");
            Secrets = JsonConvert.DeserializeObject<Dictionary<string, string>>(secretsJson);
        }
    }
}
