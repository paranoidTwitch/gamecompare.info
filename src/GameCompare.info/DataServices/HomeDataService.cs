﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using GameCompare.info.Controllers;
using GameCompare.info.Data;
using GameCompare.info.Data.Interfaces;
using GameCompare.info.DataServices.Interfaces;
using GameCompare.info.Models;
using GameCompare.info.Models.APIModels;
using GameCompare.info.Models.HomeModels;
using GameCompare.info.SteamApiSupport;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;


namespace GameCompare.info.DataServices
{
    public class HomeDataService : IHomeDataService
    {
        private ISteamDataRepository _steamDataRepository;
        
        public HomeDataService(UserManager<ApplicationUser> userManager, ISteamApi steamApi, ISteamDataRepository steamDataRepository)
        {
            _steamDataRepository = steamDataRepository;
        }

        public HomeIndexViewModel GetHomeIndexModel(string userId)
        {
            HomeIndexViewModel viewModel = new HomeIndexViewModel();

            Task steamUserInfoTask  = Task.Run(() =>
            {
                ApplicationDbContext steamUserTaskApplicationDbContext = _steamDataRepository.GetThreadingContext();

                viewModel.CurrentUserInfo = _steamDataRepository.SelectSteamInfoByUserId(steamUserTaskApplicationDbContext, userId, true);

                viewModel.FriendUserInfos = _steamDataRepository.SelectUsersFriends(steamUserTaskApplicationDbContext, userId);
            });

            Task globalCollectionsTask = Task.Run(() =>
            {
                ApplicationDbContext globalCollectionsTaskApplicationDbContext = _steamDataRepository.GetThreadingContext();

                viewModel.Categories = _steamDataRepository.SelectAllCategories(globalCollectionsTaskApplicationDbContext);

                viewModel.Genres = _steamDataRepository.SelectAllGeneres(globalCollectionsTaskApplicationDbContext);
            });

            Task friendGameOwnershipTask = Task.Run(() =>
            {
                ApplicationDbContext friendGameTaskApplicationDbContext = _steamDataRepository.GetThreadingContext();

                viewModel.Games = _steamDataRepository.SelectGamesOwnedByUser(friendGameTaskApplicationDbContext, userId, true);

                viewModel.FriendGameOwnership = _steamDataRepository.SelectFriendsGamesUnion(friendGameTaskApplicationDbContext, userId, viewModel.Games);
            });

            steamUserInfoTask.Wait();
            globalCollectionsTask.Wait();
            friendGameOwnershipTask.Wait();
            
            return viewModel;
        }
    }
}
