﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using GameCompare.info.Models.HomeModels;

namespace GameCompare.info.DataServices.Interfaces
{
    public interface IHomeDataService
    {
        HomeIndexViewModel GetHomeIndexModel(string userId);
    }
}
