﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using GameCompare.info.Data;
using GameCompare.info.Data.Interfaces;
using GameCompare.info.Data.Repositories;
using GameCompare.info.DataServices.Interfaces;
using GameCompare.info.Models.APIModels;
using GameCompare.info.Models.HomeModels;
using GameCompare.info.QuartzJobs;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Quartz;

namespace GameCompare.info.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISteamDataRepository _steamDataRepository;
        private readonly IApplicationFactRepository _applicationFactRepository;
        private readonly IHomeDataService _homeDataController;
        private readonly IScheduler _scheduler;

        public HomeController(
            ISteamDataRepository steamDataRepository, 
            IApplicationFactRepository applicationFactRepository,
            IHomeDataService dataController, 
            IScheduler scheduler)
        {
            _steamDataRepository = steamDataRepository;
            _applicationFactRepository = applicationFactRepository;
            _homeDataController = dataController;
            _scheduler = scheduler;
        }

        public IActionResult Index()
        {
            if (HttpContext.User == null ||
                !HttpContext.User.Identity.IsAuthenticated)
            {
                return Redirect("Home/GetStarted");
            }

            string userId = HttpContext.User.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier).Value;

            if (!_steamDataRepository.IsUserDataCurrent(userId))
            {
                IJobDetail job = JobBuilder.Create<BringUserDataCurrentJob>()
                    .WithIdentity($"Data update for {userId}")
                    .UsingJobData("UserId", userId)
                    .Build();

                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity($"Trigger for {userId}", "DataSync")
                    .StartNow()
                    .Build();

                Task<bool> jobLookup = _scheduler.CheckExists(job.Key);
                jobLookup.Wait();
                if (!jobLookup.Result)
                {
                    _scheduler.ScheduleJob(job, trigger).Wait();
                }
                
                return Redirect("Home/WaitForDataSync");
            }

            HomeIndexViewModel viewmodel = _homeDataController.GetHomeIndexModel(userId);

            return View(viewmodel);
        }

        public IActionResult GetStarted()
        {
            return View();
        }

        public IActionResult WaitForDataSync()
        {
            
            string userId = HttpContext.User.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier).Value;
            string baseUrl = String.Format("{0}://{1}", Request.Scheme, Request.Host);
            
            WaitForDataSyncModel viewModel = new WaitForDataSyncModel()
            {
                RedirectUrl = String.Format("{0}/", baseUrl),
                StatusAjaxUrl = String.Format("{0}/Ajax/IsUserDataReady?userId={1}", baseUrl, userId)
            };
            return View(viewModel);
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
