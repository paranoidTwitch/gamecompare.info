﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameCompare.info.Data.Interfaces;
using GameCompare.info.Models.AjaxModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace GameCompare.info.Controllers
{
    public class AjaxController
    {
        private ISteamDataRepository _steamDataRepository;

        public AjaxController(ISteamDataRepository steamDataRepository)
        {
            _steamDataRepository = steamDataRepository;
        }
        
        public string IsUserDataReady(string userId)
        {
            var returnValue = new IsUserDataReadyViewModel()
            {
                IsUserDataReady = _steamDataRepository.IsUserDataCurrent(userId)
            };
            
            return JsonConvert.SerializeObject(returnValue);
        }
    }
}
