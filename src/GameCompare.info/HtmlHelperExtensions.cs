﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace GameCompare.info
{
    public static class HtmlHelperExtensions
    {
        public static string MakeDescriptionClassNameSafe<tmodel>(this IHtmlHelper<tmodel> helper, string nameToProcess)
        {
            return nameToProcess.Replace("(", "").Replace(")", "").Replace(" ", "-").ToLower();
        }
    }
}
