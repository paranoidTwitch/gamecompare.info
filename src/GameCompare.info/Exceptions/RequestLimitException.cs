﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameCompare.info.Exceptions
{
    public class RequestLimitException : Exception
    {
        public RequestLimitException(string message) : base(message) { }
        public RequestLimitException(string message, Exception innerException) : base(message, innerException) { }
    }
}
