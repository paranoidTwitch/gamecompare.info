﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameCompare.info.Exceptions
{
    public class SteamApiException : Exception
    {
        public SteamApiException(string message) : base(message) { }
        public SteamApiException(string message, Exception innerException) : base(message, innerException) { }
    }
}
