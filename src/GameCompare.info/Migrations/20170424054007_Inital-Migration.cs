﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GameCompare.info.Migrations
{
    public partial class InitalMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GameCategoryInfo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameCategoryInfo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GameDetail",
                columns: table => new
                {
                    ApplicationId = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    HeaderImage = table.Column<string>(nullable: true),
                    IsFree = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameDetail", x => x.ApplicationId);
                });

            migrationBuilder.CreateTable(
                name: "GameGenreInfo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Desciption = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameGenreInfo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GameType",
                columns: table => new
                {
                    GameTypeId = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    GameTypeName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameType", x => x.GameTypeId);
                });

            migrationBuilder.CreateTable(
                name: "SteamUserInfo",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    Avatar = table.Column<string>(nullable: true),
                    AvatarFull = table.Column<string>(nullable: true),
                    AvatarMedium = table.Column<string>(nullable: true),
                    CommunityVisibilityState = table.Column<int>(nullable: false),
                    LastDataSyncTime = table.Column<DateTime>(nullable: false),
                    LastLogoff = table.Column<string>(nullable: true),
                    PersonaName = table.Column<string>(nullable: true),
                    PersonaState = table.Column<int>(nullable: false),
                    PersonaStateFlags = table.Column<int>(nullable: false),
                    PrimaryClanId = table.Column<string>(nullable: true),
                    ProfileState = table.Column<int>(nullable: false),
                    ProfileUrl = table.Column<string>(nullable: true),
                    RealName = table.Column<string>(nullable: true),
                    SteamId = table.Column<string>(nullable: true),
                    TimeCreated = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SteamUserInfo", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "ApplicationFact",
                columns: table => new
                {
                    ApplicationFactId = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Key = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationFact", x => x.ApplicationFactId);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    PasswordHash = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    SecurityStamp = table.Column<string>(nullable: true),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                });

            migrationBuilder.CreateTable(
                name: "GameToCategory",
                columns: table => new
                {
                    GameApplicaitonId = table.Column<int>(nullable: false),
                    GameCategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameToCategory", x => new { x.GameApplicaitonId, x.GameCategoryId });
                    table.ForeignKey(
                        name: "FK_GameToCategory_GameDetail_GameApplicaitonId",
                        column: x => x.GameApplicaitonId,
                        principalTable: "GameDetail",
                        principalColumn: "ApplicationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GameToCategory_GameCategoryInfo_GameCategoryId",
                        column: x => x.GameCategoryId,
                        principalTable: "GameCategoryInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GameToGenre",
                columns: table => new
                {
                    GameApplicationId = table.Column<int>(nullable: false),
                    GenereId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameToGenre", x => new { x.GameApplicationId, x.GenereId });
                    table.ForeignKey(
                        name: "FK_GameToGenre_GameDetail_GameApplicationId",
                        column: x => x.GameApplicationId,
                        principalTable: "GameDetail",
                        principalColumn: "ApplicationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GameToGenre_GameGenreInfo_GenereId",
                        column: x => x.GenereId,
                        principalTable: "GameGenreInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SteamUserToFriend",
                columns: table => new
                {
                    SteamUserId = table.Column<string>(nullable: false),
                    FriendUserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SteamUserToFriend", x => new { x.SteamUserId, x.FriendUserId });
                    table.ForeignKey(
                        name: "FK_SteamUserToFriend_SteamUserInfo_SteamUserId",
                        column: x => x.SteamUserId,
                        principalTable: "SteamUserInfo",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SteamUserToGame",
                columns: table => new
                {
                    GameApplicationId = table.Column<int>(nullable: false),
                    SteamUserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SteamUserToGame", x => new { x.GameApplicationId, x.SteamUserId });
                    table.ForeignKey(
                        name: "FK_SteamUserToGame_GameDetail_GameApplicationId",
                        column: x => x.GameApplicationId,
                        principalTable: "GameDetail",
                        principalColumn: "ApplicationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SteamUserToGame_SteamUserInfo_SteamUserId",
                        column: x => x.SteamUserId,
                        principalTable: "SteamUserInfo",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GameToCategory_GameApplicaitonId",
                table: "GameToCategory",
                column: "GameApplicaitonId");

            migrationBuilder.CreateIndex(
                name: "IX_GameToCategory_GameCategoryId",
                table: "GameToCategory",
                column: "GameCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_GameToGenre_GameApplicationId",
                table: "GameToGenre",
                column: "GameApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_GameToGenre_GenereId",
                table: "GameToGenre",
                column: "GenereId");

            migrationBuilder.CreateIndex(
                name: "IX_SteamUserToFriend_SteamUserId",
                table: "SteamUserToFriend",
                column: "SteamUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SteamUserToGame_GameApplicationId",
                table: "SteamUserToGame",
                column: "GameApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_SteamUserToGame_SteamUserId",
                table: "SteamUserToGame",
                column: "SteamUserId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_UserId",
                table: "AspNetUserRoles",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GameToCategory");

            migrationBuilder.DropTable(
                name: "GameToGenre");

            migrationBuilder.DropTable(
                name: "SteamUserToFriend");

            migrationBuilder.DropTable(
                name: "SteamUserToGame");

            migrationBuilder.DropTable(
                name: "GameType");

            migrationBuilder.DropTable(
                name: "ApplicationFact");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "GameCategoryInfo");

            migrationBuilder.DropTable(
                name: "GameGenreInfo");

            migrationBuilder.DropTable(
                name: "GameDetail");

            migrationBuilder.DropTable(
                name: "SteamUserInfo");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");
        }
    }
}
