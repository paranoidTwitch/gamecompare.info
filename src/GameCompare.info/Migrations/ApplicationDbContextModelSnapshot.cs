﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using GameCompare.info.Data;

namespace GameCompare.info.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    partial class ApplicationDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.3");

            modelBuilder.Entity("GameCompare.info.Data.Models.GameToCategory", b =>
                {
                    b.Property<int>("GameApplicaitonId");

                    b.Property<int>("GameCategoryId");

                    b.HasKey("GameApplicaitonId", "GameCategoryId");

                    b.HasIndex("GameApplicaitonId");

                    b.HasIndex("GameCategoryId");

                    b.ToTable("GameToCategory");
                });

            modelBuilder.Entity("GameCompare.info.Data.Models.GameToGenre", b =>
                {
                    b.Property<int>("GameApplicationId");

                    b.Property<int>("GenereId");

                    b.HasKey("GameApplicationId", "GenereId");

                    b.HasIndex("GameApplicationId");

                    b.HasIndex("GenereId");

                    b.ToTable("GameToGenre");
                });

            modelBuilder.Entity("GameCompare.info.Data.Models.SteamUserToFriend", b =>
                {
                    b.Property<string>("SteamUserId");

                    b.Property<string>("FriendUserId");

                    b.HasKey("SteamUserId", "FriendUserId");

                    b.HasIndex("SteamUserId");

                    b.ToTable("SteamUserToFriend");
                });

            modelBuilder.Entity("GameCompare.info.Data.Models.SteamUserToGame", b =>
                {
                    b.Property<int>("GameApplicationId");

                    b.Property<string>("SteamUserId");

                    b.HasKey("GameApplicationId", "SteamUserId");

                    b.HasIndex("GameApplicationId");

                    b.HasIndex("SteamUserId");

                    b.ToTable("SteamUserToGame");
                });

            modelBuilder.Entity("GameCompare.info.Models.APIModels.GameCategoryInfo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.HasKey("Id");

                    b.ToTable("GameCategoryInfo");
                });

            modelBuilder.Entity("GameCompare.info.Models.APIModels.GameDetail", b =>
                {
                    b.Property<int>("ApplicationId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("HeaderImage");

                    b.Property<bool>("IsFree");

                    b.Property<string>("Name");

                    b.Property<string>("Type");

                    b.HasKey("ApplicationId");

                    b.ToTable("GameDetail");
                });

            modelBuilder.Entity("GameCompare.info.Models.APIModels.GameGenreInfo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Desciption");

                    b.HasKey("Id");

                    b.ToTable("GameGenreInfo");
                });

            modelBuilder.Entity("GameCompare.info.Models.APIModels.GameType", b =>
                {
                    b.Property<int>("GameTypeId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("GameTypeName");

                    b.HasKey("GameTypeId");

                    b.ToTable("GameType");
                });

            modelBuilder.Entity("GameCompare.info.Models.APIModels.SteamUserInfo", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("Avatar");

                    b.Property<string>("AvatarFull");

                    b.Property<string>("AvatarMedium");

                    b.Property<int>("CommunityVisibilityState");

                    b.Property<DateTime>("LastDataSyncTime");

                    b.Property<string>("LastLogoff");

                    b.Property<string>("PersonaName");

                    b.Property<int>("PersonaState");

                    b.Property<int>("PersonaStateFlags");

                    b.Property<string>("PrimaryClanId");

                    b.Property<int>("ProfileState");

                    b.Property<string>("ProfileUrl");

                    b.Property<string>("RealName");

                    b.Property<string>("SteamId");

                    b.Property<string>("TimeCreated");

                    b.HasKey("UserId");

                    b.ToTable("SteamUserInfo");
                });

            modelBuilder.Entity("GameCompare.info.Models.ApplicationFact", b =>
                {
                    b.Property<int>("ApplicationFactId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Key");

                    b.Property<string>("Value");

                    b.HasKey("ApplicationFactId");

                    b.ToTable("ApplicationFact");
                });

            modelBuilder.Entity("GameCompare.info.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("GameCompare.info.Data.Models.GameToCategory", b =>
                {
                    b.HasOne("GameCompare.info.Models.APIModels.GameDetail", "Game")
                        .WithMany("GameToCategories")
                        .HasForeignKey("GameApplicaitonId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("GameCompare.info.Models.APIModels.GameCategoryInfo", "CategoryInfo")
                        .WithMany("CategoryToGames")
                        .HasForeignKey("GameCategoryId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("GameCompare.info.Data.Models.GameToGenre", b =>
                {
                    b.HasOne("GameCompare.info.Models.APIModels.GameDetail", "Game")
                        .WithMany("GameToGenres")
                        .HasForeignKey("GameApplicationId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("GameCompare.info.Models.APIModels.GameGenreInfo", "GenreInfo")
                        .WithMany("GenreToGames")
                        .HasForeignKey("GenereId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("GameCompare.info.Data.Models.SteamUserToFriend", b =>
                {
                    b.HasOne("GameCompare.info.Models.APIModels.SteamUserInfo", "SteamUser")
                        .WithMany("Friends")
                        .HasForeignKey("SteamUserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("GameCompare.info.Data.Models.SteamUserToGame", b =>
                {
                    b.HasOne("GameCompare.info.Models.APIModels.GameDetail", "GameDetail")
                        .WithMany("GamesToUsers")
                        .HasForeignKey("GameApplicationId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("GameCompare.info.Models.APIModels.SteamUserInfo", "SteamUser")
                        .WithMany("UsersToGames")
                        .HasForeignKey("SteamUserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("GameCompare.info.Models.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("GameCompare.info.Models.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("GameCompare.info.Models.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
